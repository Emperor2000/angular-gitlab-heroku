import { Post } from './Post'

export class Board {
  // tslint:disable-next-line:variable-name
  _id: string
  id: string
  title: string
  description: string
  posts: Post[]
  userId: string
  // array of posts
  // reference to user
}
