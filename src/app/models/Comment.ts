export class Comment {
  // tslint:disable-next-line:variable-name
  _id: string
  id: string
  content: string
  userId: string // string because of mongo object id
  postId: string // string because of mongo object id
}
