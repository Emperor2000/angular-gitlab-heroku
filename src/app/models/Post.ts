import { Comment } from './Comment'
export class Post {
  // tslint:disable-next-line:variable-name
  _id: string
  id: string
  title: string
  description: string
  userId: string // string because of mongo object id
  boardId: string // string because of mongo object id
  comments: Comment[]
}
