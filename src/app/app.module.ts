import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AppRoutingModule } from './app-routing.module'
import { FormsModule } from '@angular/forms'
import { ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { BoardsComponent } from './boards/boards.component'
import { LoginComponent } from './login/login.component'
import { LoginService } from './services/login.service'
import { ShowBoardService } from './services/show-board.service'
import { HttpClientModule } from '@angular/common/http'
import { ShowBoardComponent } from './show-board/show-board.component'
import { AddBoardService } from './services/add-board.service'
import { PostsComponent } from './posts/posts.component'
import { ShowPostComponent } from './show-post/show-post.component'
import { CommonService } from './services/common.service'
import { CommentsComponent } from './comments/comments.component'
import { ShowCommentComponent } from './show-comment/show-comment.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    BoardsComponent,
    LoginComponent,
    ShowBoardComponent,
    PostsComponent,
    ShowPostComponent,
    CommentsComponent,
    ShowCommentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [LoginService, ShowBoardService, AddBoardService, CommonService],
  bootstrap: [AppComponent]
})
export class AppModule {}
