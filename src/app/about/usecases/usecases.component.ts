import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd.'
    },
    {
      id: 'UC-02',
      name: 'Weergeven van message boards',
      description: 'Hiermee kan een gebruiker de verschillende mogelijke message boards weergeven.',
      scenario: ['De gebruiker klikt op boards', 'De gebruiker krijgt een overzicht van message boards.'],
      actor: 'Reguliere gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt.'
    },
    {
      id: 'UC-03',
      name: 'Gebruikersnaam aanpassen',
      description: 'Hiermee past een bestaande gebruiker zijn naam aan.',
      scenario: [
        'De gebruiker klikt op zijn account.',
        'De gebruiker klikt op zijn gebruikersnaam.',
        'De gebruiker vult een nieuwe gebruikersnaam in.',
        'De gebruiker klikt op wijzigingen toepassen'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De gebruiker is ingelogt.',
      postcondition: 'De actor heeft zijn gebruikersnaam aangepast.'
    },
    {
      id: 'UC-04',
      name: 'Weergeven van thuisscherm',
      description: 'Hiermee kan een gebruiker de thuispagina van de website weergeven.',
      scenario: ['De gebruiker gaat naar een search engine', 'De gebruiker typt de URL van de website in.'],
      actor: 'Reguliere gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt.'
    },
    // tslint:disable-next-line:indent

    // tslint:disable-next-line:indent

    {
      id: 'UC-05',
      name: 'Posts weergeven',
      description: 'Weergeven van posts',
      scenario: [
        'De gebruiker scrollt door een lijst van posts en kiest een post die hij leuk vindt.',
        'Hij klikt.',
        'Hij krijgt informatie over de post te zien.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De gebruiker heeft UC-02 doorlopen.',
      postcondition: 'De posts zijn weergegeven'
    },
    {
      id: 'UC-06',
      name: 'Gebruikers weergeven',
      description: 'Hiermee kan een gebruiker de verschillende gebruikers inzien.',
      scenario: [
        'De gebruiker klikt op users',
        // tslint:disable-next-line:indent
        'De gebruiker krijgt een overzicht van gebruikers.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'Het doel is bereikt.'
    },
    // tslint:disable-next-line:indent
    {
      id: 'UC-07',
      name: 'Een profiel bekijken.',
      description: 'Hiermee kan een gebruiker een profiel bekijken',
      scenario: [
        'De gebruiker klikt op een profiel',
        'De gebruiker komt op een nieuwe pagina terecht.',
        'De gebruiker ziet het account dat hij wilde tonen. (geen privé info zichtbaar)'
        // tslint:disable-next-line:indent
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De gebruiker heeft UC-06 doorlopen',
      postcondition: 'De actor heeft zijn gebruikersnaam aangepast.'
    },
    {
      id: 'UC-08',
      name: 'Een lijst van comments weergeven',
      description: 'Hiermee kan een gebruiker de verschillende comments onder een post inzien.',
      scenario: [
        'De gebruiker klikt op een post',
        'De gebruiker krijgt reacties/comments op de post te zien.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt.'
    },
    {
      id: 'UC-09',
      name: 'Een comment achterlaten.',
      description: 'Hiermee kan een gebruiker een comment achterlaten op de website.',
      scenario: [
        'De gebruiker heeft UC-05 doorlopen.',
        // tslint:disable-next-line:indent
        'De gebruiker klikt op een post.',
        'De gebruiker klikt op reageren.',
        'De gebruiker kan een bericht intypen.' + 'De gebruiker klikt op verzenden'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'Het doel is bereikt.'
    },
    // tslint:disable-next-line:indent
    {
      id: 'UC-010',
      name: 'Een post achterlaten.',
      description: 'Hiermee kan een gebruiker een post achterlaten',
      scenario: [
        'De gebruiker klikt op een  message board',
        'De gebruiker kan klikken op "Create a Post"',
        'De gebruiker type zijn bericht in.',
        'De gebruiker klikt op verzenden.',
        'Andere gebruikers kunnen reageren op de post van de gebruiker.'
        // tslint:disable-next-line:indent
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'De gebruiker heeft UC-06 doorlopen',
      postcondition: 'De actor heeft zijn doel bereikt.'
    }
  ]

  title: 'VFX Forum'

  constructor() {}

  ngOnInit() {}
}
