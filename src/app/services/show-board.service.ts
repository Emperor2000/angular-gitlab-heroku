import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Board } from '../models/Board'

@Injectable()
export class ShowBoardService {
  constructor(private http: HttpClient) {}

  getAllBoards() {
    return this.http.post('https://y2p2-cswfi-api.herokuapp.com/apiv2/boards/getAllBoards', {})
  }
}
