import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Board } from '../models/Board'

@Injectable()
export class ShowPostService {
  constructor(private http: HttpClient) {}

  getAllPosts(cb) {
    return this.http
      .get('https://y2p2-cswfi-api.herokuapp.com/apiv2/posts/' + localStorage.getItem('boardId'), {})
      .subscribe(object => {
        const obj = JSON.parse(JSON.stringify(object))
        console.log(obj)
        const posts = obj.subjects
        cb(posts)
      })
  }
}
