import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Board } from '../models/Board'
import { LoginService } from './login.service'
import { Post } from '../models/Post'
import { Comment } from '../models/Comment'
import { of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AddCommentService {
  constructor(private http: HttpClient, private loginService: LoginService) {}

  addComment(comment: Comment) {
    console.log('TOKEN IS: ' + this.loginService.token)
    console.log('token from local storage is ' + localStorage.getItem('token'))
    if (localStorage.getItem('token') && localStorage.getItem('postId')) {
      return this.http.post(
        // tslint:disable-next-line:max-line-length
        'https://y2p2-cswfi-api.herokuapp.com/apiv1/comments/createComment?token=' +
          localStorage.getItem('token') +
          '&postId=' +
          localStorage.getItem('postId'),
        {
          content: comment.content,
          userId: comment.userId
        }
      )
    } else {
      return of(null)
    } // prevent errors
  }
}
