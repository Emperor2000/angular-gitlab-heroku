import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { User } from '../models/User'
import { FormsModule } from '@angular/forms'
import { Post } from '../models/Post'
import { of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public token
  constructor(private http: HttpClient) {}

  validateLogin(user: User, cb) {
    const api = 'localhost:3000'
    const emailregex = /@/

    if (!emailregex.test(user.email)) {
      alert('Please enter a valid email address and password!')
    } else {
      // true
      this.http
        .post('https://y2p2-cswfi-api.herokuapp.com/auth/user/login', {
          email: user.email,
          password: user.password
        })
        .subscribe(object => {
          // console.log('OBJECT IS' + object)
          const obj = JSON.parse(JSON.stringify(object))
          // console.log(obj)
          this.token = obj.token
          // console.log('TOKEN AFTER LOGON: ' + this.token)
          localStorage.setItem('token', this.token)
          if (this.token !== undefined) {
            cb(obj.data[0])
          }
        })
      // console.log('token value after request ' + this.token)
    }
  }
  validateChangeUsername(user: User, cb) {
    const api = 'localhost:3000'
    const emailregex = /@/

    if (!emailregex.test(user.email)) {
      alert('Please enter a valid email address, username and password!')
    } else {
      // true
      this.http
        .put('https://y2p2-cswfi-api.herokuapp.com/auth/user/changeUsername', {
          email: user.email,
          password: user.password,
          username: user.username
        })
        .subscribe(object => {
          const obj = JSON.parse(JSON.stringify(object))
          // console.log(obj)
          // this.token = obj.token
          // // console.log('TOKEN AFTER LOGON: ' + this.token)
          // localStorage.setItem('token', this.token)
          // if (this.token !== undefined) {
          //   cb(obj.data[0])
          // }
        })
    }
  }

  validateRegister(user: User) {
    const api = 'localhost:3000'
    const emailregex = /@/
    const passwordregex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/
    if (!passwordregex.test(user.password)) {
      alert(
        'Password should be at least 8 characters long,\n' +
          'should contain:\n' +
          'at least 1 number.\n' +
          'at least 1 character.\n' +
          'at least 1 special character.'
      )
    } else if (!emailregex.test(user.email)) {
      alert('Please enter a valid email address and password!')
    } else {
      return this.http.post('https://y2p2-cswfi-api.herokuapp.com/auth/user/register', {
        username: user.username,
        password: user.password,
        email: user.email
      })
    }
  }
}
