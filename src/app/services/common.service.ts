import { Injectable } from '@angular/core'
// tslint:disable-next-line:import-spacing
import { Subject } from 'rxjs'
import { HttpClient } from '@angular/common/http'

@Injectable()
export class CommonService {
  public boardAddedObservable = new Subject()
  public postAddedObservable = new Subject()
  public commentAddedObservable = new Subject()
  constructor(private http: HttpClient) {}

  notifyBoardAddition() {
    this.boardAddedObservable.next()
  }
  notifyPostAddition() {
    this.postAddedObservable.next()
  }
  notifyCommentAddition() {
    this.commentAddedObservable.next()
  }
  // getUser(cb) {
  //   return this.http.get('https://y2p2-cswfi-api.herokuapp.com/apiv2/user/' + localStorage.getItem('token'), {}).subscribe(object => {
  //     const obj = JSON.parse(JSON.stringify(object));
  //     console.log(obj);
  //     const comments = obj.subjects;
  //     cb(comments);
  //   })
  // }
}
