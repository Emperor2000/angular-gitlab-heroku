import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Board } from '../models/Board'
import { LoginService } from './login.service'
import { Post } from '../models/Post'
import { of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AddPostService {
  constructor(private http: HttpClient, private loginService: LoginService) {}

  addPost(post: Post) {
    console.log('TOKEN IS: ' + this.loginService.token)
    console.log('token from local storage is ' + localStorage.getItem('token'))
    if (localStorage.getItem('token') && localStorage.getItem('boardId')) {
      return this.http.post(
        // tslint:disable-next-line:max-line-length
        'https://y2p2-cswfi-api.herokuapp.com/apiv1/posts/createPost?token=' +
          localStorage.getItem('token') +
          '&boardId=' +
          localStorage.getItem('boardId'),
        {
          title: post.title,
          description: post.description,
          userId: post.userId
        }
      )
    } else {
      return of(null)
    } // prevent errors
  }
}
