import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Board } from '../models/Board'
import { LoginService } from './login.service'
import { of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AddBoardService {
  constructor(private http: HttpClient, private loginService: LoginService) {}

  addBoard(board: Board) {
    console.log('TOKEN IS: ' + this.loginService.token)
    console.log('token from local storage is ' + localStorage.getItem('token'))
    if (localStorage.getItem('token')) {
      return this.http.post(
        'https://y2p2-cswfi-api.herokuapp.com/apiv1/boards/createBoard?token=' +
          localStorage.getItem('token'),
        {
          title: board.title,
          description: board.description,
          userId: board.userId
        }
      )
    } else {
      return of(null)
    } // prevent errors
  }
}
