import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Post } from '../models/Post'

@Injectable()
export class ShowCommentService {
  constructor(private http: HttpClient) {}
  //
  getAllComments(cb) {
    return this.http
      .get('https://y2p2-cswfi-api.herokuapp.com/apiv2/comments/' + localStorage.getItem('postId'), {})
      .subscribe(object => {
        const obj = JSON.parse(JSON.stringify(object))
        console.log(obj)
        const comments = obj.subjects
        cb(comments)
      })
  }
}
