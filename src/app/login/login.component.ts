import { Component } from '@angular/core'
import { LoginService } from '../services/login.service'
import { User } from '../models/User'
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'
import { FormsModule } from '@angular/forms'
import { ReactiveFormsModule } from '@angular/forms'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent {
  public user: User

  constructor(private loginService: LoginService, private router: Router) {
    this.user = new User()
  }

  validateLogin() {
    if (this.user.email && this.user.password) {
      this.loginService.validateLogin(this.user, user => {
        console.log(user)
        if (user !== undefined) {
          this.router.navigate(['/boards'])
        } else {
          alert('Something went wrong, please try again.')
        }
      })
    } else {
      alert('Enter email address and password')
    }
  }

  validateRegister() {
    if (this.user.username && this.user.password && this.user.email) {
      this.loginService.validateRegister(this.user).subscribe(
        result => {
          console.log('result is ', result)
          alert('Account succesfully created.')
          const status = 'status'
          console.log(result)
          this.loginService.validateLogin(this.user, user => {
            if (user !== undefined) {
              this.router.navigate(['/boards'])
            } else {
              alert('Something went wrong, please try again.')
            }
          })
        },
        error => {
          console.log('error is ', error)
        }
      )
    } else {
      alert('Something went wrong, please try again.')
    }
  }

  activeTokenCheck() {
    // tslint:disable-next-line:max-line-length
    if (localStorage.getItem('token') !== null) {
      return true
    } else {
      return false
    }
  }

  validateChangeUsername() {
    if (this.user.email && this.user.password && this.user.username) {
      alert('Changing username')
      this.loginService.validateChangeUsername(this.user, user => {
        console.log(user)
        if (user !== undefined) {
          alert('Username succesfully changed')
          // this.router.navigate(['/boards'])
        } else {
          alert('Something went wrong, please try again.')
        }
      })
    } else {
      alert('Enter email address and password')
    }
  }

  logoutUser() {
    localStorage.removeItem('token')
    console.log(localStorage.getItem('token'))
  }
}
