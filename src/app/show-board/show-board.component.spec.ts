import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ShowBoardComponent } from './show-board.component'
import { ShowBoardService } from '../services/show-board.service'
import { LoginComponent } from '../login/login.component'
import { LoginService } from '../services/login.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { CommonService } from '../services/common.service'
import { RouterTestingModule } from '@angular/router/testing'

describe('ShowBoardComponent', () => {
  let component: ShowBoardComponent
  let fixture: ComponentFixture<ShowBoardComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ShowBoardComponent],
      providers: [ShowBoardService, CommonService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowBoardComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
// it('should create', () => {
//   expect(true).toBeTruthy()
// })
