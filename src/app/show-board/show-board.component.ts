import { Component, OnInit } from '@angular/core'
import { ShowBoardService } from '../services/show-board.service'
import { AddBoardService } from '../services/add-board.service'
import { Router } from '@angular/router'
import { Board } from '../models/Board'
import { CommonService } from '../services/common.service'
@Component({
  selector: 'app-show-board',
  templateUrl: './show-board.component.html',
  styleUrls: ['./show-board.component.scss'],
  providers: [ShowBoardService]
})
export class ShowBoardComponent implements OnInit {
  public boards: any[]
  public posts: any[]
  constructor(
    private showBoardService: ShowBoardService,
    private router: Router,
    private commonService: CommonService
  ) {}
  ngOnInit() {
    this.getAllBoards()
    this.commonService.boardAddedObservable.subscribe(res => {
      this.getAllBoards()
    })
  }
  boardClicked(board: Board) {
    console.log('Board ID of clicked: ' + board._id)
    localStorage.setItem('boardId', board._id) // get board item TODO: Add reference to correct board id.
    this.routeToBoard()
  }

  routeToBoard() {
    console.log('TOKEN VALUE FROM STORAGE ' + localStorage.getItem('boardId'))
    this.router.navigate(['/posts/'])
    // + localStorage.getItem('boardId')
  }

  getAllBoards() {
    this.showBoardService.getAllBoards().subscribe(result => {
      const data = 'data'
      this.boards = result[data] // object access via string literals is not allowed
    })
  }
}
