import { ModuleWithProviders, NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LoginComponent } from './login/login.component'
import { BoardsComponent } from './boards/boards.component'
import { PostsComponent } from './posts/posts.component'
import { CommentsComponent } from './comments/comments.component'

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'login', component: LoginComponent },
  { path: 'boards', component: BoardsComponent },
  { path: 'posts', component: PostsComponent },
  // children: [
  //   {path: 'comments', component: CommentsComponent}
  // ]},
  { path: 'comments', component: CommentsComponent },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
// export const ROUTING: ModuleWithProviders = RouterModule.forRoot(routes)
