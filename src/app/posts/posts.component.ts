// import { Component, OnInit } from '@angular/core'
// import { Post } from '../models/Post'
// // import { AddPostService } from '../add-post.service'
//
// @Component({
//   selector: 'app-posts',
//   templateUrl: './posts.component.html',
//   styleUrls: ['./posts.component.scss']
// })
// export class PostsComponent implements OnInit {
//   public post: Post
//   showModal: boolean
//
//   // constructor(private addPostService: AddPostService) {
//   //   this.post = new Post()
//   // }
//
//   addPost() {
//     if (this.post.title && this.post.description) {
//       // call the service method to add post
//       // if (this.addPostService.addPost(this.post) === undefined) {
//       //    console.log('The user is not logged in!');
//       //    alert('You need to be logged in to do that!');
//       //   }
//       //   this.addPostService.addPost(this.post).subscribe(res => {
//       //     // response from REST API call
//       //   })
//       // } else {
//       //   alert('Title and Description required')
//     }
//   }
//   ngOnInit() {}
// }
import { Component, OnInit } from '@angular/core'
import { Post } from '../models/Post'
import { AddPostService } from '../services/add-post.service'
import { CommonService } from '../services/common.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  public post: Post
  showModal: boolean
  constructor(
    private addPostService: AddPostService,
    private commonService: CommonService,
    private router: Router
  ) {
    this.post = new Post()
  }
  returnToHome() {
    this.router.navigate(['/dashboard'])
  }

  returnToProfile() {
    this.router.navigate(['/login'])
  }

  // Go to a new page when a post is clicked
  addPost() {
    if (this.post.title && this.post.description) {
      // call the service method to add post
      if (this.addPostService.addPost(this.post) === undefined) {
        console.log('The user is not logged in!')
        alert('You need to be logged in to do that!')
      }
      this.addPostService.addPost(this.post).subscribe(res => {
        this.commonService.notifyPostAddition()
        console.log('notify post addition was called.')
        // response from REST API call
      })
    } else {
      alert('Title and Description required')
    }
  }
  ngOnInit() {}
}
