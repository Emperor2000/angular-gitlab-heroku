import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { PostsComponent } from './posts.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ShowBoardComponent } from '../show-board/show-board.component'
import { AddBoardService } from '../services/add-board.service'
import { ShowBoardService } from '../services/show-board.service'
import { FormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing'
import { CommonService } from '../services/common.service'
import { ShowPostComponent } from '../show-post/show-post.component'
import { ShowPostService } from '../services/show-post.service'

describe('PostsComponent', () => {
  let component: PostsComponent
  let fixture: ComponentFixture<PostsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, FormsModule, RouterTestingModule],
      declarations: [PostsComponent, ShowPostComponent],
      providers: [ShowPostService, CommonService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should trigger modal when add post button is clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.modalOpenTrigger')

    button.click()

    expect(component.showModal).toEqual(true)
  })

  it('should trigger modal close when close button is clicked for add post', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.modalOpenTrigger')
    const buttonClose = fixture.debugElement.nativeElement.querySelector('.modalCloseTrigger')
    button.click()
    buttonClose.click()
    expect(component.showModal).toEqual(false)
  })
})
