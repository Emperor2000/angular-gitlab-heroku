import { Component, OnInit } from '@angular/core'
import { ShowPostService } from '../services/show-post.service'
import { Router } from '@angular/router'
import { CommonService } from '../services/common.service'
import { Post } from '../models/Post'

@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styleUrls: ['./show-post.component.scss'],
  providers: [ShowPostService]
})
export class ShowPostComponent implements OnInit {
  public boards: any[]
  public posts: any[]
  showModal: boolean
  constructor(
    private showPostService: ShowPostService,
    private commonService: CommonService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getAllPosts()
    this.commonService.postAddedObservable.subscribe(res => {
      this.getAllPosts()
    })
  }

  postClicked(post: Post) {
    console.log('Post ID of clicked: ' + post._id)
    localStorage.setItem('postId', post._id) // get board item TODO: Add reference to correct post id.
    this.routeToPost()
  }

  routeToPost() {
    console.log('TOKEN VALUE FROM STORAGE ' + localStorage.getItem('postId'))
    this.router.navigate(['/comments/'])
    // + localStorage.getItem('boardId')
  }

  getAllPosts() {
    this.showPostService.getAllPosts(posts => {
      this.posts = posts
    })
    console.log(this.posts)
  }
}
