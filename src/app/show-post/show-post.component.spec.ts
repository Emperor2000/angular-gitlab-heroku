import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ShowPostComponent } from './show-post.component'
import { ShowPostService } from '../services/show-post.service'
import { LoginComponent } from '../login/login.component'
import { LoginService } from '../services/login.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { CommonService } from '../services/common.service'
import { RouterTestingModule } from '@angular/router/testing'

describe('ShowPostComponent', () => {
  let component: ShowPostComponent
  let fixture: ComponentFixture<ShowPostComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ShowPostComponent],
      providers: [ShowPostService, CommonService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
