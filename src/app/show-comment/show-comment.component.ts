import { Component, OnInit } from '@angular/core'
import { ShowCommentService } from '../services/show-comment.service'
import { CommonService } from '../services/common.service'

@Component({
  selector: 'app-show-comment',
  templateUrl: './show-comment.component.html',
  styleUrls: ['./show-comment.component.scss'],
  providers: [ShowCommentService]
})
export class ShowCommentComponent implements OnInit {
  public posts: any[]
  public comments: any[]
  showModal: boolean
  constructor(private showCommentService: ShowCommentService, private commonService: CommonService) {}

  ngOnInit() {
    this.getAllComments()
    this.commonService.commentAddedObservable.subscribe(res => {
      this.getAllComments()
    })
  }

  getAllComments() {
    this.showCommentService.getAllComments(comments => {
      this.comments = comments
    })
    console.log(this.comments)
  }
}
