import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ShowCommentComponent } from './show-comment.component'
import { ShowCommentService } from '../services/show-comment.service'
import { LoginComponent } from '../login/login.component'
import { LoginService } from '../services/login.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { CommonService } from '../services/common.service'
import { RouterTestingModule } from '@angular/router/testing'

describe('ShowCommentComponent', () => {
  let component: ShowCommentComponent
  let fixture: ComponentFixture<ShowCommentComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ShowCommentComponent],
      providers: [ShowCommentService, CommonService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowCommentComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
