import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { CommentsComponent } from './comments.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ShowBoardComponent } from '../show-board/show-board.component'
import { AddBoardService } from '../services/add-board.service'
import { ShowBoardService } from '../services/show-board.service'
import { FormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing'
import { CommonService } from '../services/common.service'
import { ShowCommentComponent } from '../show-comment/show-comment.component'
import { ShowCommentService } from '../services/show-comment.service'

describe('CommentsComponent', () => {
  let component: CommentsComponent
  let fixture: ComponentFixture<CommentsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, FormsModule, RouterTestingModule],
      declarations: [CommentsComponent, ShowCommentComponent],
      providers: [ShowCommentService, CommonService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
