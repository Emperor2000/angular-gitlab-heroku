// import { Component, OnInit } from '@angular/core';
//
// @Component({
//   selector: 'app-comments',
//   templateUrl: './comments.component.html',
//   styleUrls: ['./comments.component.scss']
// })
// export class CommentsComponent implements OnInit {
//
//   constructor() { }
//
//   ngOnInit() {
//   }
//
// }

import { Component, OnInit } from '@angular/core'
import { Comment } from '../models/Comment'
import { AddCommentService } from '../services/add-comment.service'
import { CommonService } from '../services/common.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  showModal: boolean
  public comment: Comment
  constructor(
    private addCommentService: AddCommentService,
    private commonService: CommonService,
    private router: Router
  ) {
    this.comment = new Comment()
  }
  // Go to a new page when a post is clicked
  addComment() {
    if (this.comment.content) {
      // call the service method to add post
      if (this.addCommentService.addComment(this.comment) === undefined) {
        console.log('The user is not logged in!')
        alert('You need to be logged in to do that!')
      }
      this.addCommentService.addComment(this.comment).subscribe(res => {
        this.commonService.notifyCommentAddition()
        console.log('notify post addition was called.')
        // response from REST API call
      })
    } else {
      alert('Your comment cannot be empty!')
    }
  }
  ngOnInit() {}

  returnToHome() {
    this.router.navigate(['/dashboard'])
  }

  returnToProfile() {
    this.router.navigate(['/login'])
  }
}
