import { Component, Input } from '@angular/core'
@Component({
  selector: 'app-navbar',
  template: `
    <!-- Vertical navbar -->
    <div class="vertical-nav bg-dark text-white-50" id="sidebar">
      <div class="py-4 px-3 mb-4 bg-dark">
        <div class="media d-flex align-items-center">
          <!--                <img src="https://res.cloudinary.com/mhmd/image/upload/v1556074849/avatar-1_tcnd60.png"
        alt="VFX FORUM LOGO GOES HERE"-->
          <!--                     width="65" class="mr-3 rounded-circle img-thumbnail shadow-sm">-->
          <div class="media-body inmenu">
            <h1
              class="m-0 font-weight-bolder text-white homediv navbar-brand"
              routerLink="/"
              [routerLinkActive]="['active']"
              [routerLinkActiveOptions]="{ exact: true }"
            >
              {{ title }}
            </h1>
            <p class="font-weight-light mb-0">The number 1 forum for VFX artists.</p>
          </div>
        </div>
      </div>

      <!--        <p class="text-gray font-weight-bold text-uppercase px-3 small pb-4 mb-0">Main</p>-->

      <ul class="nav flex-column bg-dark mb-0 homediv">
        <li class="nav-item inmenu">
          <a href="#" class="nav-link text-light font-italic">
            <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
            <a
              class="nav-link"
              routerLink="about"
              [routerLinkActive]="['active']"
              [routerLinkActiveOptions]="{ exact: true }"
              >About</a
            >
          </a>
        </li>
        <li class="nav-item inmenu">
          <a href="#" class="nav-link text-light font-italic">
            <i class="fa fa-cubes mr-3 text-primary fa-fw"></i>
            <a
              class="nav-link"
              routerLink="boards"
              [routerLinkActive]="['active']"
              [routerLinkActiveOptions]="{ exact: true }"
              >Boards</a
            >
          </a>
        </li>
        <li class="nav-item inmenu">
          <a href="#" class="nav-link text-light font-italic">
            <i class="fa fa-picture-o mr-3 text-primary fa-fw"></i>
            <a
              class="nav-link"
              routerLink="login"
              [routerLinkActive]="['active']"
              [routerLinkActiveOptions]="{ exact: true }"
              >Profile</a
            >
          </a>
        </li>
      </ul>

      <!--NOTE_TO_SELF: I CUT SOME CONTENT HERE, SAVED IN SCHOOL CLIENT SIDE FOLDER-->
    </div>
    <!-- End vertical navbar -->
  `,
  styles: [
    '.homediv { cursor: grab; }',
    '.btn-link { color: rgba(255,255,255,.5); text-decoration: none; }',
    // tslint:disable-next-line: max-line-length
    '.btn-link.focus, .btn-link:focus, .btn-link.hover, .btn-link:hover { color: rgba(255,255,255,.75); text-decoration: none; box-shadow: none; }'
  ]
})
export class NavbarComponent {
  @Input() title: string
  isNavbarCollapsed = true
}
