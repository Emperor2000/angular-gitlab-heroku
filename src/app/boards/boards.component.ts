import { Component, OnInit } from '@angular/core'
import { Board } from '../models/Board'
import { AddBoardService } from '../services/add-board.service'
import { CommonService } from '../services/common.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {
  public board: Board
  showModal: boolean
  constructor(
    private addBoardService: AddBoardService,
    private commonService: CommonService,
    private router: Router
  ) {
    this.board = new Board()
  }
  // Go to a new page when a board is clicked
  addBoard() {
    if (this.board.title && this.board.description) {
      // call the service method to add post
      if (this.addBoardService.addBoard(this.board) === undefined) {
        console.log('The user is not logged in!')
        alert('You need to be logged in to do that!')
      }
      this.addBoardService.addBoard(this.board).subscribe(res => {
        this.commonService.notifyBoardAddition()
        console.log('notify board addition was called.')
        // response from REST API call
      })
    } else {
      alert('Title and Description required')
    }
  }
  ngOnInit() {}

  returnToHome() {
    this.router.navigate(['/dashboard'])
  }

  returnToProfile() {
    this.router.navigate(['/login'])
  }
}
