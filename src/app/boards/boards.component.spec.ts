import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { BoardsComponent } from './boards.component'
import { ShowBoardComponent } from '../show-board/show-board.component'
import { AddBoardService } from '../services/add-board.service'
import { ShowBoardService } from '../services/show-board.service'
import { FormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing'
import { CommonService } from '../services/common.service'
import { Router } from '@angular/router'
import { DashboardComponent } from '../core/dashboard/dashboard.component'
describe('BoardsComponent', () => {
  let component: BoardsComponent
  let fixture: ComponentFixture<BoardsComponent>
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, FormsModule, RouterTestingModule],
      declarations: [BoardsComponent, ShowBoardComponent],
      providers: [AddBoardService, ShowBoardService, CommonService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })

  it('should trigger modal when add button is clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.clickModalTrigger')

    button.click()

    expect(component.showModal).toEqual(true)
  })

  it('should trigger modal close when close button is clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.clickModalTrigger')
    const buttonClose = fixture.debugElement.nativeElement.querySelector('.buttonCloseModal')
    button.click()
    buttonClose.click()
    expect(component.showModal).toEqual(false)
  })
})

describe('BoardsComponent (integrated test)', () => {
  let component: BoardsComponent
  let fixture: ComponentFixture<BoardsComponent>
  let router: Router

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, FormsModule],
      declarations: [BoardsComponent, DashboardComponent, ShowBoardComponent],
      providers: [AddBoardService, ShowBoardService, CommonService]
    }).compileComponents() // This is not needed if you are in the CLI context
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardsComponent)
    component = fixture.componentInstance

    router = TestBed.get(Router)
    spyOn(router, 'navigateByUrl')

    fixture.detectChanges()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
  it('should trigger the navigation to `/dashboard`', async(() => {
    const link = fixture.debugElement.nativeElement.querySelector('.returnToHome')
    link.click()
    expect(router.navigateByUrl).toHaveBeenCalled()
  }))

  // it('should trigger the navigation to `/home`', async(() => {
  //   const link = fixture.debugElement.nativeElement.querySelector('.returnToHome');
  //   link.click();
  //   expect(router.navigateByUrl).toHaveBeenCalled();
  // }));
})
